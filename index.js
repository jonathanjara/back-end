'use strict'

const express = require('express');
const validation = require('./controllers/validator');

const app = express();

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:false}));

//rutas
app.use(require('./routes/index'));



app.listen(30000);
console.log('Server on port 3000');