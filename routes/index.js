const{ Router} = require('express');
const  router = Router();

const {getUsers, postUsers, getUsersById, deleteUsersById, putUsers} = require('../controllers/user.controller');

router.get('/users', getUsers);
router.get('/users/:id', getUsersById);
router.post('/users', postUsers);
router.delete('/users/:id', deleteUsersById);
router.put('/users/:id', putUsers);
   

module.exports = router;