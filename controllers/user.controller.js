const { Pool } = require('pg');

const pool = new Pool({
    host:'localhost',
    port:'5432',
    user: 'postgres',
    password: 'toor',
    database: 'frenon'
});
//const validator = require('./validator');

const getUsers = async (req, res) => {
    const result = await pool.query('SELECT * FROM users');
    res.status(200).json(result.rows);
};

const getUsersById = async (req, res) => {
    const id = req.params.id;
    const result = await pool.query('SELECT * FROM users where id=$1',[id]);
    res.status(200).json(result.rows);
};

const postUsers = async (req, res)=>{
   // validator.UsersValidator(req.body);
    const {name, email, password, status} = req.body;
    const result = await pool.query('INSERT INTO users (name, email, password, status) VALUES ( $1, $2, $3, $4)',  [name, email, password, status]);
    res.json({
        message: "user created succesfully",
        body: {
            user: {
                'name': name,
                'email': email
            }
        }
    })
};

const deleteUsersById = async (req, res) => {
    const id = req.params.id;
    const result = await pool.query('Delete from users where id = $1',[id]);
    res.json({
        message: "User Deleted succesfully"
    })
};

const putUsers = async( req, res)=>{
   // validator.UsersValidator(req.body);
    const id = req.params.id;
    const { name, email, password, status} = req.body;

    const result = await pool.query('UPDATE users SET name =$1, email = $2, password =$3, status = $4 WHERE id = $5',[
        name, email, password, status, id
    ]);

    res.json({
        message: 'User id :'+id +' actualizado correctamente',
        body:{
            'name': name,
            'email': email,
            'password': password,
            'status': status
        }
    });
};



module.exports = {
    getUsers,
    postUsers,
    getUsersById,
    deleteUsersById,
    putUsers
}