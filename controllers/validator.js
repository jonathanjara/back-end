function UsersValidator(data){
    const {name, email, password, status} = data;

    if(typeof name === 'string'){
        throw new Error('var name must be a string');
    }
    if(name.length >=15 && /^[a-z]+$/i.test(name)){
        throw new Error('var name be at least 15 characters long');
    }
    if(/^[a-z]+$/i.test(name)){
        throw new Error('var name must contain only a-z charcteres');

    }

    //email
    if(typeof email === 'string'){
        throw new Error('var email must be a string');
    }
    if(/^[a-z0-9_.]+@[a-z0-9]+\.[a-z0-9_.]+$/i.test(email)){
        throw new Error('var email be valid');
    }

    //password
    if(typeof password === 'string'){
        throw new Error('var password must be a string');
    }
    if(password.length >=12 && /^[a-z]+$/i.test(password)){
        throw new Error('var password be at least 12 characters long');
    }
    if(/^[a-z0-9]+$/i.test(password)){
        throw new Error('var password must contain only a-z 0-9 charcteres');

    }
    //status
    if(typeof status === 'string'){
        throw new Error('var status must be a string');
    }
    if(status.length >=10 && /^[a-z]+$/i.test(status)){
        throw new Error('var status be at least 10 characters long');
    }
    if(/^[a-z]+$/i.test(status)){
        throw new Error('var status must contain only a-z charcteres');

    }
}

module.exports = {
    UsersValidator,
}